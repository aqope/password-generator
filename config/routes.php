<?php

return [
    'test/task' => 'TestController@Task',
    'test/submit' => 'TestController@Submit',
    '' => 'IndexController@Index',
    'pageNotFound' => 'ErrorController@Index',
];