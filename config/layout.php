<?php

return [
    'test/task' => [
        'template' => 'password.phtml',
        'class' => 'PasswordBox'
    ],
    'pageNotFound' => [
        'template' => '404.phtml',
        'class' => 'ErrorPage'
    ],
    '' => [
        'template' => 'index.phtml',
        'class' => 'IndexPage'
    ]
];