<?php

class Router
{
    protected $requested_route;
    protected $request_method;
    protected $default_url;

    public function __construct()
    {
        if (preg_match('/^(\/index.php)/', $_SERVER['REQUEST_URI'])) {
            $reqUri = preg_replace('/^(\/index.php)/', '', $_SERVER['REQUEST_URI']);

            if (preg_match('/^(\/)/', $reqUri)) {
                $reqUri = preg_replace('/^(\/)/', '', $reqUri);
            }

            $this->redirect($reqUri);
            die();
        }

        $this->request_method = $_SERVER['REQUEST_METHOD'];
        $reqRoutes = explode('/', $_SERVER['REQUEST_URI']);
        $reqRoutes = array_filter($reqRoutes);
        $this->requested_route = implode('/', $reqRoutes);
        $this->default_url = getConfig('config.defaultRoutes.index');
    }

    public function route()
    {
        $routes = getConfig('routes');
        $errorCode = 0;
        if (isset($routes[$this->requested_route])) {
            $route = explode('@', $routes[$this->requested_route]);
            $controllerName = $route[0];
            $actionName = strtolower($route[1]) . 'Action';
            if (class_exists($controllerName)) {
                $controller = new $controllerName($this);
                if (method_exists($controller, $actionName)) {
                    $controller->$actionName();
                } else {
                    $errorCode = 404;
                }
            } else {
                $errorCode = 404;
            }
        } else {
            $errorCode = 404;
        }

        if ($errorCode == 404) {
            $this->redirectTo404();
        }
    }

    public function getRequestedRoute()
    {
        return $this->requested_route;
    }

    public function getRequestMethod()
    {
        return $this->request_method;
    }

    public function redirectTo404()
    {
        $url = getConfig('config.defaultRoutes.404');
        $this->redirect($url);
    }

    public function redirect($url)
    {
        header('Location: ' . $this->formatUrl($url));
    }

    protected function formatUrl($url)
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';

        $returnUrl = $protocol . $host . '/' . $url;
        return $returnUrl;
    }

    public function getBaseUrl()
    {
        return $this->formatUrl($this->default_url);
    }

    public function getHost()
    {
        return $this->formatUrl('');
    }
}