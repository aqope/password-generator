<?php

/** Main Classes */
include_once('app/Router/Router.php');

/** Controllers */
include_once('app/Controllers/Controller.php');
include_once('app/Controllers/TestController.php');
include_once('app/Controllers/ErrorController.php');
include_once ('app/Controllers/IndexController.php');

/** Models */
include_once('app/Models/Main.php');
include_once('app/Models/Password.php');

/** Core */
include_once('app/core.php');
include_once('app/router.php');