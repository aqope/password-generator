<?php

include_once('app/Views/View.php');

class IndexPage extends View
{
    public function getToTaskUrl()
    {
        return $this->getController()->getHost() . 'test/task';
    }
}