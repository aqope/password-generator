<?php

/** Dependency */
include_once('app/Views/View.php');

class PasswordBox extends View
{
    public function isNumberActive()
    {
        return (bool)$this->data['isNumberActive'];
    }

    public function isUppercaseActive()
    {
        return (bool)$this->data['isUppercaseActive'];
    }

    public function isLowercaseActive()
    {
        return (bool)$this->data['isLowercaseActive'];
    }
}