<?php

include_once('app/Views/View.php');

class ErrorPage extends View
{
    public function getBaseUrl()
    {
        return $this->getController()->getBaseUrl();
    }
}