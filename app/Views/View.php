<?php

class View
{
    protected $data;
    protected $template;
    protected $controller;

    public function __construct($data = array(), $controller = null)
    {
        $this->controller = $controller;
        $this->data = $data;
    }

    public function getData($key = null)
    {
        if (isset($key)) {
            if (isset($this->data[$key])) {
                return $this->data[$key];
            }
                return '';
        } else {
            return $this->data;
        }
    }

    public function setData($key, $value)
    {
        if (is_string($key)) {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function render()
    {
        if (file_exists($this->template)) {
            include_once($this->template);
        } else {
            return false;
        }

        return true;
    }

    protected function getController()
    {
        return $this->controller;
    }
}