<?php

class Password extends Main
{
    public function __construct()
    {

    }

    public function generatePassword($criteria)
    {
        $i = 0;
        $passwordBox = array();

        $isNumberActive = $criteria['includeNumbers'];
        $isUppercaseActive = $criteria['includeBigLetters'];
        $isLowercaseActive = $criteria['includeSmallLetters'];

        $pregeneratedNumbers = $isNumberActive ? $this->pregenerateNumber() : false;
        $pregeneratedUppercase = $isUppercaseActive ? $this->pregenerateUppercase() : false;
        $pregeneratedLowercase = $isLowercaseActive ? $this->pregenerateLowercase() : false;

        $position = -1;
        $usedPositions = array();

        if ($pregeneratedNumbers) {
            while (in_array($position, $usedPositions) || $position == -1) {
                $position = rand(0, $criteria['length'] -1);
            }

            $passwordBox[$position] = $pregeneratedNumbers;
            $usedPositions[] = $position;
        }

        if ($pregeneratedUppercase) {
            while (in_array($position, $usedPositions) || $position == -1) {
                $position = rand(0, $criteria['length'] -1);
            }

            $passwordBox[$position] = $pregeneratedUppercase;
            $usedPositions[] = $position;
        }

        if ($pregeneratedLowercase) {
            while (in_array($position, $usedPositions) || $position == -1) {
                $position = rand(0, $criteria['length'] -1);
            }

            $passwordBox[$position] = $pregeneratedLowercase;
            $usedPositions[] = $position;
        }

        while ($i < $criteria['length']) {
            if (!in_array($i, $usedPositions)) {
                $method = rand(1,3);
                $generated = 0;
                if ($method == 1 && $isNumberActive) {
                    $generated = $this->pregenerateNumber();
                } elseif ($method == 2 && $isUppercaseActive) {
                    $generated = $this->pregenerateUppercase();
                } elseif ($method == 3 && $isLowercaseActive) {
                    $generated = $this->pregenerateLowercase();
                } else {
                    continue;
                }
                if (!in_array($generated, $passwordBox)) {
                    $passwordBox[$i] = $generated;
                } else {
                    continue;
                }

            }
            $i++;
        }

        $passwordBox = array_map(function($item) {
            return chr($item);
        }, $passwordBox);

        ksort($passwordBox);
        return implode('', $passwordBox);
    }

    protected function pregenerateNumber()
    {
        $number = rand(48, 57);

        if ($number == 48 || $number == 49) {
            $number = $this->pregenerateNumber();
        }

        return $number;
    }

    protected function pregenerateUppercase()
    {
        /* Big Letters without o and O ? */
        $number = rand(65, 90);

        if ($number == 79) {
            $number = $this->pregenerateUppercase();
        }

        return $number;
    }

    protected function pregenerateLowercase()
    {
        $number = rand(97, 122);

        if ($number == 108) {
            $number = $this->pregenerateLowercase();
        }

        return $number;
    }
}