<?php

class Controller
{
    protected $router;
    protected $template_path = 'app/Templates';
    protected $default_view = 'View';

    public function __construct($router)
    {
        session_start();
        $this->router = $router;
    }

    protected function render($data = array())
    {
        $reqRoute = $this->router->getRequestedRoute();
        $template = getConfig('layout' . '.' . $reqRoute);
        if (isset($template)) {
            if (file_exists($this->template_path . '/' . $template['template'])) {
                if (isset($template['class'])) {
                    if (file_exists('app/Views/' . $template['class'] . '.php')) {
                        include_once('app/Views/' . $template['class'] . '.php');
                    } else {
                        throw new Exception('Selected Class File does not exist!');
                    }

                    $view = new $template['class']($data, $this);
                } else {
                    if (file_exists('app/Views'. '/' . $this->default_view . '.php')) {
                        include_once('app/Views'. '/' . $this->default_view . '.php');
                    } else {
                        throw new Exception('Default View Class does not exist!');
                    }

                    $view = new $this->default_view($data, $this);
                }
                $view->setTemplate($this->template_path . '/' . $template['template']);
                $view->render();
            } else {
                $this->router->redirectTo404();
            }
        } else {
            $this->router->redirectTo404();
        }
    }

    public function getBaseUrl()
    {
        return $this->router->getBaseUrl();
    }

    public function getHost()
    {
        return $this->router->getHost();
    }
}