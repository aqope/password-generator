<?php

class TestController extends Controller
{
    public function taskAction()
    {
        $data = array(
            'password' => isset($_SESSION['generatedPassword']) ? $_SESSION['generatedPassword'] : '',
            'isNumberActive' => isset($_SESSION['isNumberActive']) ? $_SESSION['isNumberActive'] : false,
            'isUppercaseActive' => isset($_SESSION['isUppercaseActive']) ? $_SESSION['isUppercaseActive'] : false,
            'isLowercaseActive' => isset($_SESSION['isLowercaseActive']) ? $_SESSION['isLowercaseActive'] : false,
            'length' => isset($_SESSION['passwordLength']) ? $_SESSION['passwordLength'] : 0,
        );

        $this->render($data);
    }

    public function submitAction()
    {
        if ($this->router->getRequestMethod() === 'POST') {

            $post = [
                'includeNumbers' => isset($_POST['rule1']) ? true : false,
                'includeBigLetters' => isset($_POST['rule2']) ? true : false,
                'includeSmallLetters' => isset($_POST['rule3']) ? true : false,
                'length' => (int)$_POST['length']
            ];

            $maxLength = 0;

            $maxLength = $post['includeNumbers'] ? $maxLength + 8 : $maxLength + 0;
            $maxLength = $post['includeBigLetters'] ? $maxLength + 25 : $maxLength + 0;
            $maxLength = $post['includeSmallLetters'] ? $maxLength + 25 : $maxLength + 0;

            $minLength = 0;

            $minLength = $post['includeNumbers'] ? $minLength + 1 : $minLength + 0;
            $minLength = $post['includeBigLetters'] ? $minLength + 1 : $minLength + 0;
            $minLength = $post['includeSmallLetters'] ? $minLength + 1 : $minLength + 0;

            if ($post['length'] >= $maxLength) {
                throw new Exception('Selected Password Length is too big! Please select less or equals than ' . $maxLength . '!');
            }

            if ($post['length'] <= 0) {
                throw new Exception('Selected Password Length is equals to 0! Please select more or equals than ' . $minLength . '!');
            }

            if ($post['length'] < $minLength) {
                throw new Exception('Selected Password Length is too small! Please select more or equals than ' . $minLength . '!');
            }

            $password = new Password();

            $generatedPassword = $password->generatePassword($post);

            $_SESSION['generatedPassword'] = $generatedPassword;
            $_SESSION['isNumberActive'] = $post['includeNumbers'];
            $_SESSION['isUppercaseActive'] = $post['includeBigLetters'];
            $_SESSION['isLowercaseActive'] = $post['includeSmallLetters'];
            $_SESSION['passwordLength'] = $post['length'];


            $this->router->redirect('test/task');
        }
    }
}